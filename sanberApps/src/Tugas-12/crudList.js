import React from 'react';

class CrudList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataHargaBuah: [
                                {nama: "Semangka", harga: 10000, berat: 1000},
                                {nama: "Anggur", harga: 40000, berat: 500},
                                {nama: "Strawberry", harga: 30000, berat: 400},
                                {nama: "Jeruk", harga: 30000, berat: 1000},
                                {nama: "Mangga", harga: 30000, berat: 500}
                            ],
            inputNama: "",
            inputHarga: "",
            inputBerat: ""
        }

    }

    changeInputNama = (event) => {
        this.setState({
            inputNama: event.target.value
        })
    }

    changeInputHarga = (event) => {
        this.setState({
            inputHarga: event.target.value
        })
    }

    changeInputBerat = (event) => {
        this.setState({
            inputBerat: event.target.value
        })
    }

    changeHandle = (event) => {
        event.preventDefault();
        var newobj = {nama: this.state.inputNama, harga: this.state.inputHarga, berat: this.state.inputBerat};
        console.log(newobj);
        this.setState({
            dataHargaBuah: this.dataHargaBuah[this.index] = newobj
        })
    }

    editForm = (event) => {
        var index = event.target.value;
        this.setState({
            inputNama : this.state.dataHargaBuah[index].nama,
            inputHarga : this.state.dataHargaBuah[index].harga,
            inputBerat : this.state.dataHargaBuah[index].berat
        })
        console.log(this.state.inputNama);
        
    }

    render() {
        return (
            <>
                <div class="containerDua">
                    <h1>Table Harga Buah</h1>
                    <table border="1" class="tableHarga">
                        <tr>
                            <td>No</td>
                            <td>Nama</td>
                            <td>Harga</td>
                            <td>Berat</td>
                            <td>Aksi</td>
                        </tr>
                        {
                            this.state.dataHargaBuah.map((el, index) => {
                                return (     
                                    <tr>
                                        <td>{index+1}</td>
                                        <td>{el.nama}</td>
                                        <td>{el.harga}</td>
                                        <td>{el.berat / 1000} kg</td>
                                        <td>
                                            <button value={index} onClick={this.editForm}>edit</button>
                                        </td>
                                    </tr>        
                                )
                            }
                            )
                        }
                    </table>
                    <br />
                    <form onSubmit={this.changeHandle}>
                        <table>                        
                            <tr>
                                <td><label>Nama buah</label></td>
                                <td><input required type="text" value={this.state.inputNama} onChange={this.changeInputNama}/></td>
                            </tr>
                            <tr>
                                <td><label>Harga</label></td>
                                <td><input required type="text" value={this.state.inputHarga} onChange={this.changeInputHarga}/></td>
                            </tr>
                            <tr>
                                <td><label>Berat</label></td>
                                <td><input required type="text" value={this.state.inputBerat} onChange={this.changeInputBerat}/></td>
                            </tr>
                            <tr>
                                <td colSpan="2" align="center"><button>submit</button></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </>
        ) 
    }
    

}


export default CrudList;