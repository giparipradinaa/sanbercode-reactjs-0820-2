import React from 'react';

class Jam extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: new Date().toLocaleTimeString()
        };
      }

    componentDidMount() {
        this.jamID = setInterval(
            () => this.tick(),
            1000
        );
    }
    
    componentWillUnmount() {
        clearInterval(this.jamID);
    }
    
    tick() {
        this.setState({
            time: new Date().toLocaleTimeString()
        });
    }

    render() {
        return (
            <>
                <h1 style={{textAlign: "center"}}>
                    sekarang jam : {this.state.time}
                </h1>
            </>
        )
    }

}

class Countdown extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 100,
            showElement: true 
        }
    }

    componentDidMount() {
        this.intervalID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentDidUpdate() {
        if(this.state.time === 0) {
            this.componentWillUnmount()
        }
    }

    componentWillUnmount() {
        this.state.showElement = false;
    }

    tick() {
        this.setState({
            time: this.state.time - 1
        });
    }

    render() {
        return(
            <>
            {this.state.showElement === true && (
                <>
                <div>
                    <Jam />
                    <h1 style={{textAlign: "center"}}>
                        hitung mundur: {this.state.time}
                    </h1>
                </div>
                </>
            )}   
            </>
        );
      }

}

export default Countdown;