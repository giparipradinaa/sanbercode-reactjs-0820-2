import React from 'react';
import './App.css';
import './Tugas-9/form.css';
import './Tugas-10/TableHarga.css';
// import Form from './Tugas-9/form.js';
// import TableHarga from './Tugas-10/TableHarga.js';
// import Countdown from './Tugas-11/countdown.js';
import CrudList from './Tugas-12/crudList.js';

function App() {
  return (
    <div className="App">
      
      {/* <Form />
      <TableHarga />
      <Countdown /> */}
      <CrudList />

    </div>
  );
}

export default App;
