import React from 'react';

let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ];

class TableHarga extends React.Component {
    render() {
        return  <div class="containerDua">
                    <h1>Table Harga Buah</h1>
                    <table border="1" class="tableHarga">
                        <tr>
                            <td>Nama</td>
                            <td>Harga</td>
                            <td>Berat</td>
                        </tr>
                        {
                            dataHargaBuah.map(el => {
                                return (     
                                    <tr>
                                        <td>{el.nama}</td>
                                        <td>{el.harga}</td>
                                        <td>{el.berat / 1000} kg</td>
                                    </tr>        
                                )
                            }
                            )
                        }
                    </table>
                </div>
    }
}



export default TableHarga;